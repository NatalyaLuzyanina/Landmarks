//
//  ModuleBuilderProtocol.swift
//  Landmarks
//
//  Created by Nataly on 20.12.2021.
//

import UIKit

protocol ModuleBuilder {
    func build(usingNavigationFactory factory: NavigationFactory) -> UIViewController
}
