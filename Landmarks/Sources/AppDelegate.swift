//
//  AppDelegate.swift
//  Landmarks
//
//  Created by Nataly on 14.12.2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let controller = CatalogBuilder().build(usingNavigationFactory: NavigationBuilder.build(rootView:))
        window?.rootViewController = controller
        return true
    }
}

