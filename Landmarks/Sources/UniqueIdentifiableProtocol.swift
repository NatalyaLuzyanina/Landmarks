//
//  UniqueIdentifiableProtocol.swift
//  Landmarks
//
//  Created by Nataly on 20.12.2021.
//

import Foundation

typealias UniqueIdentifier = Int

protocol UniqueIdentifiable {
    var uid: UniqueIdentifier { get }
}


