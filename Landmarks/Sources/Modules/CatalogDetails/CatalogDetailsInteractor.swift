//
//  detail informatoin about landmark module
//  Created by NatalyaLuzyanina on 23/12/2021.
//

protocol CatalogDetailsBusinessLogic {
    func fetchDetails(request: CatalogDetails.ShowDetails.Request)
}

/// Класс для описания бизнес-логики модуля CatalogDetails
class CatalogDetailsInteractor: CatalogDetailsBusinessLogic {
    let presenter: CatalogDetailsPresentationLogic
    let provider: CatalogDetailsProviderProtocol
    
    init(presenter: CatalogDetailsPresentationLogic, provider: CatalogDetailsProviderProtocol = CatalogDetailsProvider()) {
        self.presenter = presenter
        self.provider = provider
    }
    
    // MARK: Do something
    func fetchDetails(request: CatalogDetails.ShowDetails.Request) {
        provider.getItems(landmarkId: request.landmarkId) { [weak self] (items, error) in
            let result: CatalogDetails.CatalogDetailsRequestResult
            if let items = items {
                result = .success(items)
            } else if let error = error {
                result = .failure(.someError(message: error.localizedDescription))
            } else {
                result = .failure(.someError(message: "No Data"))
            }
            self?.presenter.presentFetchedDetails(response: CatalogDetails.ShowDetails.Response(result: result))
        }
    }
}
