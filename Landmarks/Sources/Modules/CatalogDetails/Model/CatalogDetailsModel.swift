//
//  Created by NatalyaLuzyanina on 23/12/2021.
//

struct CatalogDetailsModel: UniqueIdentifiable {
    let uid: UniqueIdentifier
    let title: String
    let imageName: String
    var isFavorite: Bool
    let coordinates: Coordinate
    let state: String
    let park: String
}
