//
//  Created by NatalyaLuzyanina on 23/12/2021.
//

protocol CatalogDetailsProviderProtocol {
    func getItems(landmarkId: Int,completion: @escaping (CatalogModel?, CatalogDetailsProviderError?) -> Void)
}

enum CatalogDetailsProviderError: Error {
    case getItemsFailed(underlyingError: Error)
}

struct CatalogDetailsProvider: CatalogDetailsProviderProtocol {
    let dataStore: CatalogDetailsDataStore
    let service: CatalogDetailsServiceProtocol
    
    init(dataStore: CatalogDetailsDataStore = CatalogDetailsDataStore(), service: CatalogDetailsServiceProtocol = CatalogDetailsService()) {
        self.dataStore = dataStore
        self.service = service
    }
    
    func getItems(landmarkId: Int, completion: @escaping (CatalogModel?, CatalogDetailsProviderError?) -> Void) {
        service.fetchItems(landmarkId: landmarkId) { (array, error) in
            if let error = error {
                completion(nil, .getItemsFailed(underlyingError: error))
            } else if let models = array {
                completion(models, nil)
            }
        }
    }
}
