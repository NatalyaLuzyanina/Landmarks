//
//  Created by NatalyaLuzyanina on 23/12/2021.
//

protocol CatalogDetailsServiceProtocol {
    func fetchItems(landmarkId: Int, completion: @escaping (CatalogModel?, Error?) -> Void)
}

/// Получает данные для модуля CatalogDetails
class CatalogDetailsService: CatalogDetailsServiceProtocol {
    
    func fetchItems(landmarkId: Int, completion: @escaping (CatalogModel?, Error?) -> Void) {
        guard let models = CatalogDataStore.shared.models else { return }
        for model in models {
            if model.uid == landmarkId {
                completion(model, nil)
            }
        }
    }
}
