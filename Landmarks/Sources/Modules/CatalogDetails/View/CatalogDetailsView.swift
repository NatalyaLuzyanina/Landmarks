//
//  Created by NatalyaLuzyanina on 23/12/2021.
//

import UIKit
import MapKit
import SnapKit

extension CatalogDetailsView {
    struct Appearance {
        let exampleOffset: CGFloat = 10
    }
}

class CatalogDetailsView: UIView {
    let appearance = Appearance()
    
    let mapView = MKMapView()
    
    let imageContainer: UIView = {
        let imageContainer = UIView()
        imageContainer.clipsToBounds = false
        imageContainer.layer.shadowColor = UIColor.black.cgColor
        imageContainer.layer.shadowOpacity = 0.5
        imageContainer.layer.shadowOffset = .zero
        imageContainer.layer.shadowRadius = 15
        return imageContainer
    }()
    
    let landmarkImage: UIImageView = {
        let landmarkImage = UIImageView()
        landmarkImage.clipsToBounds = true
        landmarkImage.layer.borderWidth = 5
        landmarkImage.layer.borderColor = UIColor.white.cgColor
        return landmarkImage
    }()
    
    let name: UILabel = {
        let name = UILabel()
        name.font = name.font.withSize(30)
        return name
    }()
    
    let star: UIImageView = {
        let star = UIImageView()
        star.image = UIImage(systemName: "star.fill")
        star.tintColor = .yellow
        star.contentMode = .scaleAspectFit
        return star
    }()
    
    let park: UILabel = {
        let park = UILabel()
        park.adjustsFontSizeToFitWidth = true
        park.minimumScaleFactor = 0.5
        return park
    }()
    
    let state = UILabel()
    
    override init(frame: CGRect = CGRect.zero) {
        super.init(frame: frame)
        backgroundColor = .white
        
        setImage()
        addSubviews()
        makeConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews(){
        addSubview(mapView)
        addSubview(imageContainer)
        addSubview(name)
        addSubview(star)
        addSubview(park)
        addSubview(state)
    }
    
    func makeConstraints() {
        mapView.snp.makeConstraints { maker in
            maker.top.leading.trailing.equalTo(self)
            maker.height.equalTo(self.frame.height / 2)
        }
        imageContainer.snp.makeConstraints { maker in
            maker.center.equalToSuperview()
            maker.height.width.equalTo(self.snp.width).multipliedBy(0.6)
        }
        name.snp.makeConstraints { maker in
            maker.leading.equalTo(20)
            maker.top.equalTo(imageContainer.snp.bottom).offset(30)
            maker.trailing.equalTo(star.snp.leading).offset(-8)
        }
        star.snp.makeConstraints { maker in
            maker.top.equalTo(imageContainer.snp.bottom).offset(30)
            maker.height.equalTo(name.snp.height)
        }
        state.snp.makeConstraints { maker in
            maker.top.equalTo(name.snp.bottom)
            maker.trailing.equalTo(-20)
        }
        park.snp.makeConstraints { maker in
            maker.leading.equalTo(20)
            maker.top.equalTo(name.snp.bottom)
            maker.trailing.lessThanOrEqualTo(state.snp.leading).offset(-8)
        }
    }
    
    func setImage() {
        landmarkImage.frame = CGRect(x: 0, y: 0, width: self.frame.width * 0.6, height: self.frame.width * 0.6)
        landmarkImage.layer.cornerRadius = self.frame.width * 0.6 / 2
        imageContainer.addSubview(landmarkImage)
    }
    
    func configureView(with viewModel: CatalogDetailsModel) {
        landmarkImage.image = UIImage(named: viewModel.imageName)
        name.text = viewModel.title
        star.isHidden = viewModel.isFavorite ? false : true
        park.text = viewModel.park
        state.text = viewModel.state
        mapView.setLocation(withCoordinates: viewModel.coordinates)
    }
}
