//
//  detail informatoin about landmark module
//  Created by NatalyaLuzyanina on 23/12/2021.
//

enum CatalogDetails {
    // MARK: Use cases
    enum ShowDetails {
        struct Request {
            let landmarkId: Int
        }
        
        struct Response {
            var result: CatalogDetailsRequestResult
        }
        
        struct ViewModel {
            var state: ViewControllerState
        }
    }
    
    enum CatalogDetailsRequestResult {
        case failure(CatalogDetailsError)
        case success(CatalogModel)
    }
    
    enum ViewControllerState {
        case initial(id: UniqueIdentifier)
        case loading
        case result(CatalogDetailsModel)
        case emptyResult
        case error(message: String)
    }
    
    enum CatalogDetailsError: Error {
        case someError(message: String)
    }
}
