//
//  detail informatoin about landmark module
//  Created by NatalyaLuzyanina on 23/12/2021.
//

import UIKit

protocol CatalogDetailsDisplayLogic: AnyObject {
    func displayFetchedDetails(viewModel: CatalogDetails.ShowDetails.ViewModel)
}

class CatalogDetailsViewController: UIViewController {
    let interactor: CatalogDetailsBusinessLogic
    var state: CatalogDetails.ViewControllerState
    
    var customView: CatalogDetailsView? {
        return view as? CatalogDetailsView
    }
    
    init(interactor: CatalogDetailsBusinessLogic, initialState: CatalogDetails.ViewControllerState) {
        self.interactor = interactor
        self.state = initialState
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View lifecycle
    override func loadView() {
        let view = CatalogDetailsView(frame: UIScreen.main.bounds)
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        display(newState: state)
    }
    
    func applyLoadingState(id: UniqueIdentifier) {
        fetchDetailInfo(landmarkId: id)
    }
    
    // MARK: Do something
    
    func fetchDetailInfo(landmarkId: Int) {
        let request = CatalogDetails.ShowDetails.Request(landmarkId: landmarkId)
        interactor.fetchDetails(request: request)
    }
    func setNavigationBar(title: String) {
        navigationItem.title = title
        navigationController?.navigationBar.prefersLargeTitles = false
    }
}

extension CatalogDetailsViewController: CatalogDetailsDisplayLogic {
    func displayFetchedDetails(viewModel: CatalogDetails.ShowDetails.ViewModel) {
        display(newState: viewModel.state)
    }
    
    func display(newState: CatalogDetails.ViewControllerState) {
        state = newState
        switch state {
        case .loading:
            print("loading...")
        case let .error(message):
            print("error \(message)")
        case let .result(items):
            customView?.configureView(with: items)
            setNavigationBar(title: items.title)
        case .emptyResult:
            print("empty result")
        case .initial(id: let id):
            applyLoadingState(id: id)
        }
    }
}
