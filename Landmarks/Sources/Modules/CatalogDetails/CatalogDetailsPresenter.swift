//
//  detail informatoin about landmark module
//  Created by NatalyaLuzyanina on 23/12/2021.
//

import UIKit

protocol CatalogDetailsPresentationLogic {
    func presentFetchedDetails(response: CatalogDetails.ShowDetails.Response)
}

/// Отвечает за отображение данных модуля CatalogDetails
class CatalogDetailsPresenter: CatalogDetailsPresentationLogic {
    weak var viewController: CatalogDetailsDisplayLogic?
    
    // MARK: Do something
    func presentFetchedDetails(response: CatalogDetails.ShowDetails.Response) {
        var viewModel: CatalogDetails.ShowDetails.ViewModel
        
        switch response.result {
        case let .failure(error):
            viewModel = CatalogDetails.ShowDetails.ViewModel(state: .error(message: error.localizedDescription))
        case let .success(result):
            let landmarkViewModel = detailsViewModel(from: result)
            viewModel = CatalogDetails.ShowDetails.ViewModel(state: .result(landmarkViewModel))
        }
        viewController?.displayFetchedDetails(viewModel: viewModel)
    }
    
    func detailsViewModel(from model: CatalogModel) -> CatalogDetailsModel {
        let viewModel = CatalogDetailsModel(
            uid: model.uid,
            title: model.name,
            imageName: model.imageName,
            isFavorite: model.isFavorite,
            coordinates: model.coordinates,
            state: model.state,
            park: model.park
        )
        return viewModel
    }
}
