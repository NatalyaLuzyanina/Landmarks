//
//  NavigationBuilder.swift
//  Landmarks
//
//  Created by Nataly on 23.12.2021.
//

import UIKit

typealias NavigationFactory = (UIViewController) -> (UINavigationController)

class NavigationBuilder {
    static func build(rootView: UIViewController) -> UINavigationController {
        UINavigationController(rootViewController: rootView)
    }
}
