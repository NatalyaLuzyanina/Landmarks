//
//  table of landmarks module
//  Created by NatalyaLuzyanina on 20/12/2021.
//

import UIKit

protocol CatalogPresentationLogic {
    func presentItems(response: Catalog.FetchItems.Response)
}

class CatalogPresenter: CatalogPresentationLogic {
    weak var viewController: CatalogDisplayLogic?
    
    func presentItems(response: Catalog.FetchItems.Response) {
        var viewModel: Catalog.FetchItems.ViewModel
        
        switch response.result {
        case let .failure(error):
            viewModel = Catalog.FetchItems.ViewModel(state: .error(message: error.localizedDescription))
        case let .success(result):
            if result.isEmpty {
                viewModel = Catalog.FetchItems.ViewModel(state: .emptyResult)
            } else {
                var landmarkViewModel = viewModels(from: result)
                if response.presentOnlyFavorite {
                    landmarkViewModel = filterViewModels(from: landmarkViewModel)
                }
                viewModel = Catalog.FetchItems.ViewModel(state: .result(landmarkViewModel))
            }
        }
        viewController?.displayItems(viewModel: viewModel)
    }
    
    func viewModels(from models: [CatalogModel]) -> [CatalogViewModel] {
        var viewModels = [CatalogViewModel]()
        for model in models {
            let viewModel = CatalogViewModel(
                uid: model.uid,
                title: model.name,
                imageName: model.imageName,
                isFavorite: model.isFavorite
            )
            viewModels.append(viewModel)
        }
        return viewModels
    }
    
    func filterViewModels(from models: [CatalogViewModel]) -> [CatalogViewModel] {
        let favoriteModels = models.filter { $0.isFavorite == true }
        return favoriteModels
    }
}
