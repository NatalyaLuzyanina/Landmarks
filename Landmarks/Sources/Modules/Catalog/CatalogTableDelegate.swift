//
//  CatalogTableDelegate.swift
//  Landmarks
//
//  Created by Nataly on 22.12.2021.
//
import UIKit

class CatalogTableDelegate: NSObject, UITableViewDelegate {
    
    weak var delegate: CatalogViewControllerDelegate?
    
    var representableViewModels: [CatalogViewModel]
    
    init(viewModels: [CatalogViewModel] = []) {
        representableViewModels = viewModels
    }
    let headerView = HeaderView()
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer { tableView.deselectRow(at: indexPath, animated: true) }
        guard let viewModel = representableViewModels[safe: indexPath.row] else { return }
        delegate?.openLandmarkDetails(viewModel.uid)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}


