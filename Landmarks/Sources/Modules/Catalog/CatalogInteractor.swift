//
//  table of landmarks module
//  Created by NatalyaLuzyanina on 20/12/2021.
//

protocol CatalogBusinessLogic {
    func fetchItems(request: Catalog.FetchItems.Request)
}

class CatalogInteractor: CatalogBusinessLogic {
    let presenter: CatalogPresentationLogic
    let provider: CatalogProviderProtocol
    
    init(presenter: CatalogPresentationLogic, provider: CatalogProviderProtocol = CatalogProvider()) {
        self.presenter = presenter
        self.provider = provider
    }

    func fetchItems(request: Catalog.FetchItems.Request) {
        
        provider.getItems { (items, error) in
            let result: Catalog.CatalogRequestResult
            if let items = items {
                result = .success(items)
            } else if let error = error {
                result = .failure(.someError(message: error.localizedDescription))
            } else {
                result = .failure(.someError(message: "No Data"))
            }
            
            switch request.type {
            case .landmarks:
                self.presenter.presentItems(response: Catalog.FetchItems.Response(result: result, presentOnlyFavorite: false))
            case .favoriteLandmarks:
                self.presenter.presentItems(response: Catalog.FetchItems.Response(result: result, presentOnlyFavorite: true))
            }
        }
    }
}
