//
//  table of landmarks module
//  Created by NatalyaLuzyanina on 20/12/2021.
//

import UIKit

class CatalogBuilder: ModuleBuilder {    
    
    var initialState: Catalog.ViewControllerState?
    
    func set(initialState: Catalog.ViewControllerState) -> CatalogBuilder {
        self.initialState = initialState
        return self
    } 
    
    func build(usingNavigationFactory factory: NavigationFactory) -> UIViewController {
        let presenter = CatalogPresenter()
        let interactor = CatalogInteractor(presenter: presenter)
        let controller = CatalogViewController(interactor: interactor)
        controller.title = "Landmarks"
        
        presenter.viewController = controller
        return factory(controller)
    }
}
