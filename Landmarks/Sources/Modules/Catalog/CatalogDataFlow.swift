//
//  table of landmarks module
//  Created by NatalyaLuzyanina on 20/12/2021.
//

enum Catalog {
    // MARK: Use cases
    enum FetchItems {
        struct Request {
            let type: RequestType
        }
        
        struct Response {
            var result: CatalogRequestResult
            let presentOnlyFavorite: Bool
        }
        
        struct ViewModel {
            var state: ViewControllerState
        }
    }
    
    enum RequestType {
        case landmarks
        case favoriteLandmarks
    }
    
    enum CatalogRequestResult {
        case failure(CatalogError)
        case success([CatalogModel])
    }
    
    enum ViewControllerState {
        case loading
        case result([CatalogViewModel])
        case emptyResult
        case error(message: String)
    }
    
    enum CatalogError: Error {
        case someError(message: String)
    }
}
