//
//  table of landmarks module
//  Created by NatalyaLuzyanina on 20/12/2021.
//

import UIKit

protocol CatalogDisplayLogic: AnyObject {
    func displayItems(viewModel: Catalog.FetchItems.ViewModel)
}

protocol CatalogViewControllerDelegate: AnyObject {
    func openLandmarkDetails(_ landmarkId: UniqueIdentifier)
}

class CatalogViewController: UIViewController {
    let interactor: CatalogBusinessLogic
    var state: Catalog.ViewControllerState
    var tableDataSource = CatalogTableDataSource()
    var tableHandler: CatalogTableDelegate = CatalogTableDelegate()
    lazy var customView = self.view as? CatalogView
    
    init(interactor: CatalogBusinessLogic, initialState: Catalog.ViewControllerState = .loading) {
        self.interactor = interactor
        self.state = initialState
        super.init(nibName: nil, bundle: nil)
        tableHandler.delegate = self
        tableHandler.headerView.switchDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View lifecycle
    override func loadView() {
        let view = CatalogView(frame: UIScreen.main.bounds, tableDataSource: tableDataSource, tableDelegate: tableHandler, refreshDelegate: self)
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        display(newState: state)
    }
    
    // MARK: Fetch landmarks
    func fetchItems() {
        let request = Catalog.FetchItems.Request(type: .landmarks)
        interactor.fetchItems(request: request)
    }
}
// MARK: DisplayLogic
extension CatalogViewController: CatalogDisplayLogic {
    func displayItems(viewModel: Catalog.FetchItems.ViewModel) {
        display(newState: viewModel.state)
    }
    
    func display(newState: Catalog.ViewControllerState) {
        state = newState
        switch state {
        case .loading:
            fetchItems()
        case let .error(message):
            print("error \(message)")
        case let .result(items):
            tableDataSource.representableViewModels = items
            tableHandler.representableViewModels = items
            customView?.updateTableViewData()
        case .emptyResult:
            print("empty result")
        }
    }
}

// MARK: ErrorViewDelegate
extension CatalogViewController: CatalogErrorViewDelegate {
    func reloadButtonWasTapped() {
        display(newState: .loading)
    }
}

// MARK: ViewControllerDelegate
extension CatalogViewController: CatalogViewControllerDelegate {
    func openLandmarkDetails(_ landmarkId: UniqueIdentifier) {
        let detailsController = CatalogDetailsBuilder().set(initialState: .initial(id: landmarkId)).build(usingNavigationFactory: NavigationBuilder.build(rootView:))
        navigationController?.pushViewController(detailsController, animated: true)
    }
}

// MARK: SwitchDelegate
extension CatalogViewController: CatalogSwitchDelegate {
    func showFavoriteItems() {
        let request = Catalog.FetchItems.Request(type: .favoriteLandmarks)
        interactor.fetchItems(request: request)
    }
}
