//
//  Created by NatalyaLuzyanina on 20/12/2021.
//

struct CatalogModel: UniqueIdentifiable, Decodable {
    let uid: UniqueIdentifier
    let name: String
    let category: String
    let city: String
    let state: String
    let park: String
    let coordinates: Coordinate
    let imageName: String
    var isFavorite: Bool
    
    enum CodingKeys: String, CodingKey {
        case uid = "id"
        case name = "name"
        case category = "category"
        case city = "city"
        case state = "state"
        case park = "park"
        case coordinates = "coordinates"
        case imageName = "imageName"
        case isFavorite = "isFavorite"
    } 
}

struct Coordinate: Codable {
    let longitude: Double
    let latitude: Double
}

extension CatalogModel: Equatable {
    static func == (lhs: CatalogModel, rhs: CatalogModel) -> Bool {
        return lhs.uid == rhs.uid
    }
}



