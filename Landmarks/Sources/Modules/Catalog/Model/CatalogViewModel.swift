//
//  CatalogViewModel.swift
//  Landmarks
//
//  Created by Nataly on 21.12.2021.
//

struct CatalogViewModel {
    let uid: UniqueIdentifier
    let title: String
    let imageName: String
    var isFavorite: Bool
}
