//
//  HeaderView.swift
//  Landmarks
//
//  Created by Nataly on 24.12.2021.
//

import UIKit

protocol CatalogSwitchDelegate {
    func showFavoriteItems()
    func fetchItems()
}

class HeaderView: UIView {
    var switchDelegate: CatalogSwitchDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        addSubview()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let topView: UIView = {
        let topView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 1))
        topView.backgroundColor = UIColor.systemGray5
        return topView
    }()
    
    let bottomView: UIView = {
        let bottomView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 1))
        bottomView.backgroundColor = UIColor.systemGray5
        return bottomView
    }()
    
    var switchFavorite: UISwitch = {
        let switcher = UISwitch()
        switcher.addTarget(self, action: #selector(showFavorite), for: .touchUpInside)
        return switcher
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.text = "Favorites only"
        return label
    }()
    
    let stack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .center
        return stack
    }()
    
    func addSubview() {
        stack.addArrangedSubview(label)
        stack.addArrangedSubview(switchFavorite)
        addSubview(topView)
        addSubview(stack)
        addSubview(bottomView)
    }
    
    func makeConstraints() {
        
        stack.snp.makeConstraints { maker in
            maker.leading.equalTo(self).offset(20)
            maker.trailing.equalTo(self).offset(-20)
            maker.top.equalTo(self)
            maker.bottom.equalTo(self)
        }
        topView.snp.makeConstraints { maker in
            maker.top.equalTo(self.snp.top)
            maker.height.equalTo(1)
            maker.leading.equalTo(self.snp.leading)
            maker.trailing.equalTo(self.snp.trailing)
        }
        bottomView.snp.makeConstraints { maker in
            maker.bottom.equalTo(self.snp.bottom)
            maker.height.equalTo(1)
            maker.leading.equalTo(self.snp.leading)
            maker.trailing.equalTo(self.snp.trailing)
        }
    }
    
    @objc func showFavorite() {
        if switchFavorite.isOn {
            switchDelegate?.showFavoriteItems()
        } else {
            switchDelegate?.fetchItems()
        }
    }
}
