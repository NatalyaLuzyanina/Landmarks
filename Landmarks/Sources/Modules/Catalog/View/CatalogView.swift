//
//  Created by NatalyaLuzyanina on 20/12/2021.
//

import UIKit

class CatalogView: UIView {
    struct Appearance {
        let errorTitle = "Something went wrong"
        let emptyTitle = "Nothing to do here"
        let spinnerColor: UIColor = .black
        let exampleOffset: CGFloat = 10
    }
    
    var appearance = Appearance()
    
    var tableView: UITableView
    
    lazy var emptyView: CatalogEmptyView = {
        let view = CatalogEmptyView()
        view.title.text = self.appearance.emptyTitle
        return view
    }()
    
    lazy var errorView: CatalogErrorView = {
        let view = CatalogErrorView()
        view.title.text = self.appearance.errorTitle
        view.delegate = self.refreshActionsDelegate
        return view
    }()
    
    lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .medium)
        spinner.startAnimating()
        spinner.color = appearance.spinnerColor
        spinner.hidesWhenStopped = true
        return spinner
    }()
    
    weak var refreshActionsDelegate: CatalogErrorViewDelegate?

    init(frame: CGRect = CGRect.zero,
         tableDataSource: UITableViewDataSource,
         tableDelegate: UITableViewDelegate,
         refreshDelegate: CatalogErrorViewDelegate,
         appearance: Appearance = Appearance()) {
        tableView = UITableView()
        super.init(frame: frame)
        tableView.dataSource = tableDataSource
        tableView.delegate = tableDelegate
        refreshActionsDelegate = refreshDelegate
        
        backgroundColor = UIColor.white
        configureTableView()
        addSubviews()
        makeConstraints()
        
        emptyView.isHidden = true
        tableView.isHidden = true
        errorView.isHidden = true
        spinner.isHidden = true
    }
    
    func configureTableView() {
        tableView.rowHeight = 60
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func addSubviews(){
        addSubview(tableView)
        addSubview(emptyView)
        addSubview(errorView)
        addSubview(spinner)
    }

    func makeConstraints() {
        spinner.snp.makeConstraints { maker in
            maker.center.equalToSuperview()
        }
        tableView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
        emptyView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
        errorView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
    }
    
    func showEmptyView() {
        show(view: emptyView)
    }
    
    func showLoading() {
        show(view: spinner)
    }
    
    func showError() {
        show(view: errorView)
    }
    
    func showTable() {
        show(view: tableView)
    }
    
    func show(view: UIView) {
        subviews.forEach { $0.isHidden = (view != $0) }
    }
    
    func updateTableViewData() {
        showTable()
        tableView.reloadData()
    }
}
