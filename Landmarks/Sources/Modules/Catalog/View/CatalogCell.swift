//
//  CatalogCell.swift
//  Landmarks
//
//  Created by Nataly on 22.12.2021.
//

import UIKit

class CatalogCell: UITableViewCell {
    
    lazy var title = UILabel()
    let image = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    let star = UIImageView(image: UIImage(systemName: "star.fill"))
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(cellRepresentable: CatalogViewModel) {
        title.text = cellRepresentable.title
        
        self.accessoryType = .disclosureIndicator
        
        image.image = UIImage(named: cellRepresentable.imageName)
        image.layer.cornerRadius = image.frame.width / 2
        image.clipsToBounds = true
        
        star.tintColor = .yellow
        star.isHidden = cellRepresentable.isFavorite ? false : true
    }
    
    func setupViews() {
        contentView.addSubview(title)
        contentView.addSubview(image)
        contentView.addSubview(star)
    }
    
    func setupConstraints() {
        image.snp.makeConstraints { maker in
            maker.width.equalTo(image.frame.height)
            maker.top.equalTo(5)
            maker.bottom.equalTo(-5)
            maker.leading.equalTo(20)
        }
        title.snp.makeConstraints { maker in
            maker.top.bottom.equalToSuperview()
            maker.leading.equalTo(image.snp.trailing).offset(20)
        }
        star.snp.makeConstraints { maker in
            maker.centerY.equalTo(self.snp.centerY)
            maker.leading.equalTo(title.snp.trailing)
        }
    }
}
