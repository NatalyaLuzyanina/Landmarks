//
//  Created by NatalyaLuzyanina on 20/12/2021.
//
import Foundation

protocol CatalogServiceProtocol {
    func fetchItems(completion: @escaping ([CatalogModel]?, Error?) -> Void)
}

class CatalogService: CatalogServiceProtocol {
    
    func fetchItems(completion: @escaping ([CatalogModel]?, Error?) -> Void) {
        if let path = Bundle.main.path(forResource: "landmarkData", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                do {
                    let models = try JSONDecoder().decode([CatalogModel].self, from: data)
                    completion(models, nil)
                } catch {
                    print(error.localizedDescription)
                }
            } catch {
                print(error.localizedDescription)
            }
        }  
    }
}
