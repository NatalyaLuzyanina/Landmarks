//
//  Created by NatalyaLuzyanina on 20/12/2021.
//

protocol StoresCatalogModels: AnyObject {
    var models: [CatalogModel]? { get set }
}

class CatalogDataStore {
    static let shared = CatalogDataStore()
    var models: [CatalogModel]?
}
