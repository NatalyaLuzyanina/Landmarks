//
//  Created by NatalyaLuzyanina on 20/12/2021.
//

protocol CatalogProviderProtocol {
    func getItems(completion: @escaping ([CatalogModel]?, CatalogProviderError?) -> Void)
}

enum CatalogProviderError: Error {
    case getItemsFailed(underlyingError: Error)
}

struct CatalogProvider: CatalogProviderProtocol {
    let dataStore: CatalogDataStore
    let service: CatalogServiceProtocol
    
    init(dataStore: CatalogDataStore = CatalogDataStore(), service: CatalogServiceProtocol = CatalogService()) {
        self.dataStore = dataStore
        self.service = service
    }
    
    func getItems(completion: @escaping ([CatalogModel]?, CatalogProviderError?) -> Void) {
        if dataStore.models?.isEmpty == false {
            return completion(self.dataStore.models, nil)
        }
        service.fetchItems { (array, error) in
            if let error = error {
                completion(nil, .getItemsFailed(underlyingError: error))
            } else if let models = array {
                CatalogDataStore.shared.models = models
                //нужна ли эта строка
                self.dataStore.models = models
                completion(self.dataStore.models, nil)
            }
        }
    }
}
