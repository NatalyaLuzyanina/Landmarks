//
//  UITableView + extension.swift
//  Landmarks
//
//  Created by Nataly on 20.12.2021.
//

import UIKit
extension UITableView {
    static func defaultReuseId(for elementType: UIView.Type) -> String {
        return String(describing: elementType)
    }
    
    func dequeueReusableCellWithRegistration<T: UITableViewCell>(type: T.Type, indexPath: IndexPath, reuseId: String? = nil) -> T {
        let normalizedReuseId = reuseId ?? UITableView.defaultReuseId(for: T.self)
        
        if let cell = dequeueReusableCell(withIdentifier: normalizedReuseId) as? T {
            return cell
        }
        register(type, forCellReuseIdentifier: normalizedReuseId)
        return dequeueReusableCell(withIdentifier: normalizedReuseId, for: indexPath) as! T
    }
}
