//
//  MapView.swift
//  Landmarks
//
//  Created by Nataly on 24.12.2021.
//

import MapKit

extension MKMapView {
    func setLocation(withCoordinates coordinates: Coordinate, regionRadius: CLLocationDistance = 3000) {
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}
