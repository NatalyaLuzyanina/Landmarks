//
//  Array + extension.swift
//  Landmarks
//
//  Created by Nataly on 20.12.2021.
//

import UIKit

extension Array {
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}

